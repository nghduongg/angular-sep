import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-job-form-modal',
  templateUrl: './job-form-modal.component.html',
  styleUrls: ['./job-form-modal.component.css']
})
export class JobFormModalComponent implements OnInit {
  jobForm: FormGroup;
  fields: string[] = ['IT', 'Finance', 'Marketing', 'HR', 'Sales'];

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<JobFormModalComponent>
  ) {
    this.jobForm = this.fb.group({
      company: ['', Validators.required],
      workPosition: ['', Validators.required],
      field: ['', Validators.required],
      salary: ['', Validators.required],
      workLocation: ['', Validators.required],
      jobDescription: ['', Validators.required],
      expireDate: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  ngOnInit(): void {}

  onCancel(): void {
    this.dialogRef.close();
  }
}
