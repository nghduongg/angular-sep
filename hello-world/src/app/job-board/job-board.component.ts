import { Component } from '@angular/core';
import {FormControl, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatDialog } from '@angular/material/dialog';
import { JobFormModalComponent } from '../job-form-modal/job-form-modal.component';
import { CandidateFormModalComponent } from '../candidate-form-modal/candidate-form-modal.component';


@Component({
  selector: 'app-job-board',
  templateUrl: './job-board.component.html',
  styleUrls: ['./job-board.component.css'],
})
export class JobBoardComponent {
  cards = [
    { company: 'FPT Software',position: 'Fullstack Developer', salary: '10-20 trieu VND', location: 'Ha Noi', expriedDate :'15/7/2024'},
    { company: 'FPT Software',position: 'Fullstack Developer', salary: '10-20 trieu VND', location: 'Ha Noi', expriedDate :'15/7/2024'},
    { company: 'FPT Software',position: 'Fullstack Developer', salary: '10-20 trieu VND', location: 'Ha Noi', expriedDate :'15/7/2024'},
    { company: 'FPT Software',position: 'Fullstack Developer', salary: '10-20 trieu VND', location: 'Ha Noi', expriedDate :'15/7/2024'},
    { company: 'FPT Software',position: 'Fullstack Developer', salary: '10-20 trieu VND', location: 'Ha Noi', expriedDate :'15/7/2024'},

  ];

  fields = new FormControl('');

  fieldList: string[] = ['Technology','Multimedia', 'Makerting', 'Business'];

  constructor(public dialog: MatDialog) {}

  openJobForm(): void {
    const dialogRef = this.dialog.open(JobFormModalComponent, {
      width: '600px'
    });

    
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Form data:', result);
      }
    });
  }
  openCandidateFormModal(): void {
    const dialogRef = this.dialog.open(CandidateFormModalComponent, {
      width: '600px'
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('The form was submitted with the following data:', result);
      }
    });
  }
}
  