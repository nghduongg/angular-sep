import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-candidate-form-modal',
  templateUrl: './candidate-form-modal.component.html',
  styleUrls: ['./candidate-form-modal.component.css']
})
export class CandidateFormModalComponent implements OnInit {
  candidateForm: FormGroup;
  fileName: string = '';

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<CandidateFormModalComponent>
  ) {
    this.candidateForm = this.fb.group({
      fullname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      cv: ['', Validators.required],
      jobLetter: ['', Validators.required]
    });
  }

  ngOnInit(): void {}

  onFileChange(event: any) {
    const file = event.target.files[0];
    if (file) {
      this.fileName = file.name;
      this.candidateForm.patchValue({
        cv: file
      });
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
